'''
eigen: 特征
'''
import numpy as np

A = np.array([
    [1, 2, 3],
    [4, 5, 6], 
    [7, 8 ,9]
])

print(A)
#计算A的特征向量
values, vectors = np.linalg.eig(A)

print(values)
print(vectors)
_lambda = values[0]
x = vectors[:, 0]
print('λ * x')
print(_lambda * x)
print('A * x')
print(A @ x)

#特征值组成的对角阵
l2 = np.diag(values)
#特征分解
f = vectors @ l2 @ np.linalg.inv(vectors)
print(f)


B = np.array([
    [1, 2],
    [2, 4]
])

#B是对称矩阵时， Q是正交矩阵， Q * Q.T = I
values2, Q = np.linalg.eig(B)
I = Q @ Q.T
print(I)
print( Q @ np.diag(values2) @ Q.T)