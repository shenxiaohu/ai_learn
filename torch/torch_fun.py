'''
    torch 的函数
'''
import torch

#生成向量， 长度 len(x)
x = torch.arange(4)
print(x)
print(x.shape)

#生成矩阵
A = torch.arange(20).reshape(5, 4)
print(A)

#张量
B = torch.tensor([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

C = torch.arange(20, dtype= torch.float32).reshape(5, 4)
D = C.clone()
print(C.sum())
#按列求和
print(C.sum(axis = 0))
#按行求和
print(C.sum(axis = 1))

print(C / C.sum())
#求均值
print(C.mean())

x = torch.arange(4.0)
x.requires_grad_(True)
#向量乘法
y = 2 * torch.dot(x, x)
print(y)
#调用反向传播函数
y.backward()
print(x.grad)