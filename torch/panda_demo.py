#  -*- coding: UTF-8 -*-

import pandas as pd
import os
import sys
#切换到当前路径
os.chdir(sys.path[0])

data = pd.read_csv("./test.csv")
print(data)

inputs, outputs = data.iloc[:, 0:2], data.iloc[:, 2] 
#mean(0) 即按轴方向求平均，得到每列数据的平均值. f.mean(1)则代表按行方向求平均，得到每行数据的平均值
#均值替换NaN项目
inputs = inputs.fillna(inputs.select_dtypes(include = 'number').mean())
print(inputs)

# 在对变量进行独热编码
inputs = pd.get_dummies(inputs, columns=['Alley'], prefix_sep = "_",  dummy_na = True, drop_first= False)
print(inputs)

data2 = pd.DataFrame({"x": [1, 2, 3, 4, 5], "季节": ['春', '夏', '秋', '冬', '夏']})

inputs = pd.get_dummies(data2, columns = ['季节'], dummy_na = False, drop_first= False)
print(inputs)