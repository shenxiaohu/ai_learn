#  -*- coding: UTF-8 -*-
'''
    结巴分词： pip install jieba
'''

import sys
import os
import jieba

sent = '上面的三种结构对于 RNN 的输入和输出个数都有一定的限制，但实际中很多任务的序列的长度是不固定的，例如机器翻译中，源语言、目标语言的句子长度不一样；对话系统中，问句和答案的句子长度不一样。'
wordlist = jieba.cut(sent, cut_all = True)
str = " | ".join(wordlist)
print(str)