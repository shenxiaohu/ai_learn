#  -*- coding: UTF-8 -*-
'''
    pip3 install thulac
    清华分词工具
'''
import thulac
model = thulac.thulac(seg_only=True)
str = "Gensim中的算法，如Word2Verc、FastText、潜在语义索引（LSI、LSA、LsiModel）、潜在狄利克雷分配（LDA、LdaModel）等，通过检查训练文档语料库中的统计共现模式，自动发现文档的语义结构。这些算法是无监督的，这意味着不需要人工输入——你只需要一个纯文本文档的语料库。"
print(model.cut(str, True).split())