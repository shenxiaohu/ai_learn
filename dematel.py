
import numpy as np
import numpy.matlib
'''
    Decision-making Trial and Evaluation Laboratory(DEMATEL)
    决策实验室分析。
    确定要素间的因果关系和每个要素在系统中的地位
    参考： https://baike.baidu.com/item/%E5%86%B3%E7%AD%96%E8%AF%95%E9%AA%8C%E5%92%8C%E8%AF%84%E4%BB%B7%E8%AF%95%E9%AA%8C%E6%B3%95/23608638?fr=ge_ala
'''

#直接矩阵
M = np.matrix([[0, 10, 0, 30, 100], [0, 0, 50, 0, 0], [0, 0, 0, 0, 10], [0, 0, 20, 0, 60], [0, 0, 0, 0, 0]])
#归一化，得到规范化矩阵 
#其他规范化： 对各行和各列求最大值，然后获取其中的最大值λ，N = (1/λ)*M
N = M / np.linalg.norm(M)
#计算综合影响矩阵
I = np.matlib.identity(5, dtype = float)
T = N * np.linalg.inv(I-N)
#影响度
D = np.sum(numpy.array(T), axis=1)
#被影响度
C = np.sum(numpy.array(T), axis=0)
#中心度
M2 = D + C
#原因度
R = D - C
print(T)
print(D)
print(C)
print(M2)
print(R)