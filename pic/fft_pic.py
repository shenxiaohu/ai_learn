''' 
    安装opencv
    pip install opencv-python
'''
import cv2 
import numpy as np 
import matplotlib.pyplot as plt
import os
import sys
#切换到当前路径
os.chdir(sys.path[0])
 
img = cv2.imread('./images/pic1.jpg',0)
#快速傅里叶变换 
f = np.fft.fft2(img) 
#将零频率成分移动到频域图像的中心位置。
fshift = np.fft.fftshift(f) 
#将值调整到[0, 255]的灰度空间内
magnitude_spectrum = 20*np.log(np.abs(fshift)) 
plt.subplot(121) 
plt.imshow(img, cmap = 'gray') 
plt.title('original') 
plt.axis('off') 
plt.subplot(122) 
plt.imshow(magnitude_spectrum, cmap = 'gray') 
plt.title('result') 
plt.axis('off') 
plt.show()