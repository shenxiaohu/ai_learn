'''
    softmax 函数，归一化，输出范围(0, 1)
'''
import numpy as np
def softmax(a):
    #分别计算每个元素已e为底的指数
    exp_a = np.exp(a)
    sum_exp_a = np.sum(exp_a)
    print(sum_exp_a)
    print(exp_a)
    #round:返回一个浮点数的四舍五入值
    y = [round(i/sum_exp_a,2) for i in exp_a]
    return y

a = [2, 1, 0.1]
print(softmax(a))