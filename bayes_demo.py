'''
    安装sklearn:
    pip install -U scikit-learn
'''

from sklearn import preprocessing
import pandas as pd
import numpy as np

from sklearn.naive_bayes import GaussianNB

wheather = ['Sunny', 'Sunny', 'Overcast', 'Rainy', 'Rainy', 'Rainy', 'Overcast', 'Sunny', 'Sunny', 'Rainy', 'Sunny', 'Overcast', 'Overcast', 'Rainy']
temp     = ['Hot',   'Hot',   'Hot',      'Mild',  'Cool',  'Cool',  'Cool',     'Mild',  'Cool',  'Mild',  'Mild',  'Mild',     'Hot',      'Mild' ]
play     = ['No',    'No',    'Yes',       'Yes',  'Yes',   'No',    'Yes',       'No',    'Yes',   'Yes',  'Yes',   'Yes',      'Yes',      'No'   ]

le = preprocessing.LabelEncoder()
wheather_encoded = le.fit_transform(wheather)
temp_encoded = le.fit_transform(temp)
label = le.fit_transform(play)

df1 = pd.DataFrame(wheather_encoded, columns= ['wheather'])
df2 = pd.DataFrame(temp_encoded, columns = ['temp'])
result = pd.concat([df1, df2], axis = 1, sort = False)
#朴素贝叶斯模型
model = GaussianNB()
trainx = np.array(result)
model.fit(trainx, label)
predicted = model.predict([[0, 2]]) 

print("Predicted value: ", predicted)