'''
    激活函数
'''
import numpy as np

def sigmoid(input):
    return 1.0 / (1.0 + np.exp(-input))


print(sigmoid(5))